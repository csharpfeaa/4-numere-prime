﻿using System;

namespace Numere_Prime
{
    class Program
    {
        /*
         * Scrieti o functie care verifica daca un numar este prim
         * 
         * 1. Preluati de la utilizator un numar si afisati daca este prim sau nu
         * 2. Preluati de la ultizator un numar si afisati toate numerele prime mai mici decat numarul introdus
         */
        static void Main(string[] args)
        {
            // verificam daca un numar este prim
            int numar = 30;

            if (EsteNumarPrim(numar))
                Console.WriteLine(numar + " este numar prim");
            else
                Console.WriteLine(numar + " NU este numar prim");


            // preluam de la utilizator un numar
            // afisam toate numerele prime care sunt mai mici decat numarul preluat
            int limita;

            do
            {
                Console.WriteLine("Introduceti limita pentru afisarea numerelor prime: ");

            } while (int.TryParse(Console.ReadLine(), out limita) == false);

            Console.Write("Numerele prime pana la " + limita + " sunt: ");

            for(int i = 0; i <= limita; i++)
            {
                if (EsteNumarPrim(i))
                    Console.Write(i + ", ");
            }

            Console.ReadKey();
        }

        static bool EsteNumarPrim(int numar)
        {
            if (numar == 1)
                return false;

            if (numar == 2)
                return true;

            if (numar % 2 == 0)
                return false;

            for (int i = 3; i<=numar/2; i++)
            {
                if (numar % i == 0)
                    return false;
            }

            return true;
        }
    }
}